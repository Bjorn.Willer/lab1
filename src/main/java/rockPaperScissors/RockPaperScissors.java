package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    Random random = new Random();

    int randomhandindex = random.nextInt(3);

    String cmphand = rpsChoices.get(randomhandindex);


    
    
    public void run() {
        // TODO: Implement Rock Paper Scissors

        while (true){ 

            System.out.println("Let's play round "+roundCounter);

            String humanhand = checkHand();

            if (isWinner(humanhand, cmphand)){
                System.out.println("Human chose " +humanhand + ", computer chose " + cmphand + ". Human wins!");
                humanScore++;
                System.out.println("Score: human "+humanScore+", computer "+computerScore);
                }
            else if (isWinner(cmphand, humanhand)){
                System.out.println("Human chose " +humanhand + ", computer chose " + cmphand + ". Computer wins!");
                computerScore++;
                System.out.println("Score: human "+humanScore+", computer "+computerScore);
                }
            else{  System.out.println("Human chose " +humanhand + ", computer chose " + cmphand + ". It's a tie!");
                    System.out.println("Score: human "+humanScore+", computer "+computerScore);
                    
                 }
            roundCounter++;
            
            

            if (continuePlaying().equals("n")){
                System.out.println("Bye bye :)");
                break;
            }
            else {
                continue;
            }

                }}
                

           

            
            
           
        
           
        
            
    
            



    

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    } 

    public Boolean isWinner(String one, String two){ 
        if (one.equals(rpsChoices.get(0))){
            return two.equals(rpsChoices.get(2));
             }
        else if (one.equals(rpsChoices.get(1))) {

            return two.equals(rpsChoices.get(0));
        }
        else return two.equals(rpsChoices.get(1));
     }

    public String continuePlaying(){
        List<String> possibleAnswers = Arrays.asList("y","n");
        

        while (true){ 
            System.out.println("Do you wish to continue playing? (y/n)?");

            String answer = sc.nextLine();

        if (possibleAnswers.contains(answer)){
            return answer;
        }
        else{ 
            System.out.println("I don't understand "+ answer + ". Try again");
            continue;
        
        
        }

        }
    }
    public String checkHand(){
        while (true){
            
        
            System.out.println("Your choice (Rock/Paper/Scissors)?");
        
            String humanhand = sc.nextLine().toLowerCase();
            
        if (rpsChoices.contains(humanhand)){

            return humanhand;
        }
        else {
            
                
                System.out.println("I do not understand "+humanhand+". Could you try again?");




            }
        }
    }
}  
        
    

